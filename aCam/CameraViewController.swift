//
//  ViewController.swift
//  aCam
//
//  Created by Yaroslav Kupyak on 4/20/16.
//  Copyright © 2016 Yaroslav Kupyak. All rights reserved.
//

import UIKit
import Metal
import FCFileManager
import AVFoundation


enum ModeCapture {
    case Photo
    case Video
}


class CameraViewController: UIViewController {

    
    let camera = Camera()
    
    @IBOutlet weak var buttonSwitch: UIButton!
    @IBOutlet weak var buttonTrigger: UIButton!
    @IBOutlet weak var buttonTorch: UIButton!
    @IBOutlet weak var buttonFlash: UIButton!
    @IBOutlet weak var buttonSessionPresset: UIButton!
    @IBOutlet weak var buttonModeCapture: UIButton!
    @IBOutlet weak var labelModeCapture: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    
    private var currentModeCapture: ModeCapture = .Photo
    private var frames = Array<UIImage>()
    
    @IBAction func changeModeCapture(sender: AnyObject) {
        let alertController = UIAlertController(title: "Mode capture", message: "Change the capture mode photo / video", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        alertController.addAction(UIAlertAction(title: "Photo", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.labelModeCapture.text = "Photo"
            self.labelDuration.hidden = true
            self.currentModeCapture = .Photo
        }))
        
        alertController.addAction(UIAlertAction(title: "Video", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.labelModeCapture.text = "Video"
            self.currentModeCapture = .Video
        }))
                
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func changeDetectionMode(sender: AnyObject) {
        let detectionCompatible = self.camera.compatibleDetectionMetadata()
        
        let alertController = UIAlertController(title: "Metadata Detection", message: "Change the metadata detection type", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for currentDetectionMode in detectionCompatible {
            alertController.addAction(UIAlertAction(title: currentDetectionMode.description(), style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                self.camera.metadataDetection = currentDetectionMode
            }))
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func changeFocusCamera(sender: AnyObject) {
        let focusCompatible = self.camera.compatibleCameraFocus()
        
        let alertController = UIAlertController(title: "Camera focus", message: "Change the focus camera mode, compatible with yours device", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for currentFocusMode in focusCompatible {
            alertController.addAction(UIAlertAction(title: currentFocusMode.description(), style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                self.camera.cameraFocus = currentFocusMode
            }))
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func changePressetCameraPhoto() {
        let pressetCompatible = self.camera.compatibleSessionPresset()
        
        let alertController = UIAlertController(title: "Session presset", message: "Change the presset of the session, compatible with yours device", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for currentPresset in pressetCompatible {
            alertController.addAction(UIAlertAction(title: currentPresset.foundationPreset(), style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                self.camera.sessionPresset = currentPresset
            }))
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func changePressetVideoEncoder() {
        let pressetCompatible = self.camera.compatibleVideoEncoderPresset()
        
        let alertController = UIAlertController(title: "Video encoder presset", message: "Change the video encoder presset, to change the resolution of the ouput video.", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for currentPresset in pressetCompatible {
            alertController.addAction(UIAlertAction(title: currentPresset.description(), style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                self.camera.videoEncoderPresset = currentPresset
            }))
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func changePressetSession(sender: AnyObject) {
        switch self.currentModeCapture {
        case .Photo: self.changePressetCameraPhoto()
        case .Video: self.changePressetVideoEncoder()
        }
    }
    
    @IBAction func changeTorchMode(sender: AnyObject) {
        let alertController = UIAlertController(title: "Torch mode", message: "Change the torch mode", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        alertController.addAction(UIAlertAction(title: "On", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.torchMode = .On
        }))
        alertController.addAction(UIAlertAction(title: "Off", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.torchMode = .Off
        }))
        alertController.addAction(UIAlertAction(title: "Auto", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.torchMode = .Auto
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func changeFlashMode(sender: AnyObject) {
        let alertController = UIAlertController(title: "Flash mode", message: "Change the flash mode", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        alertController.addAction(UIAlertAction(title: "On", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.flashMode = .On
        }))
        alertController.addAction(UIAlertAction(title: "Off", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.flashMode = .Off
        }))
        alertController.addAction(UIAlertAction(title: "Auto", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            self.camera.flashMode = .Auto
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func captureVideo() {
        print("record video")
        if self.camera.isRecording == false {
            guard let url = CameraFileManager.documentPath("video.mp4") else {
                return
            }
            
            self.camera.startRecordingVideo(url, blockCompletion: { (url, error) -> (Void) in
                print("url movie : \(url)")
                
                let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("previewController")
                
                CameraFileManager.saveVideo(url!, blockCompletion: { (success, error) -> (Void) in
                    print("error saving video : \(error)")
                })
                
                (controller as! PreviewViewController).media = Media.Video(url: url!)
                self.presentViewController(controller, animated: true, completion: nil)
            })
        }
        else {
            self.camera.stopRecordingVideo()
        }
    }
    
    private func capturePhoto() {
        self.camera.capturePhoto { (image: UIImage?, error: NSError?) -> (Void) in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let image = image {
                    
                    
                        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("previewController")
                        
                        CameraFileManager.savePhoto(image, blockCompletion: { (success, error) -> (Void) in
                            print("error save image : \(error)")
                        })
                        
                        (controller as! PreviewViewController).media = Media.Photo(image: image)
                        self.presentViewController(controller, animated: true, completion: nil)
                    
                }
            })
        }
    }
    
    @IBAction func captureModeChanged(sender: AnyObject) {
        if let segmentedControl = sender as? UISegmentedControl{
            if segmentedControl.selectedSegmentIndex == 0 {
                self.currentModeCapture = .Photo
                self.labelDuration.hidden = true
            }
            else{
                self.currentModeCapture = .Video
            }
        }
        
    }
    @IBAction func capturePhoto(sender: AnyObject) {
        switch self.currentModeCapture {
        case .Photo: self.capturePhoto()
        case .Video: self.captureVideo()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = event!.allTouches()!.first {
            let position = touch.locationInView(self.view)
            self.camera.focus(position)
        }
    }
    
    @IBAction func switchCamera(sender: AnyObject) {
        self.camera.switchCurrentDevice()
    }
    
    override func viewDidLayoutSubviews() {
        let layer = self.camera.previewLayer
        
        layer.frame = self.view.bounds
        self.view.layer.insertSublayer(layer, atIndex: 0)
        self.view.layer.masksToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor()
        
        self.labelDuration.hidden = true
        
        self.camera.startSession()
        
        self.camera.blockCompletionProgress = { progress in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.labelDuration.hidden = false
                self.labelDuration.text = String(format: "%.2f", progress)
            })
            print("progress duration : \(progress)")
        }
        
        self.camera.blockCompletionFaceDetection = { faceObject in
            print("face Object")
            
            (faceObject as AVMetadataObject).bounds
        }
        
        self.camera.blockCompletionCodeDetection = { codeObject in
            print("code object value : \(codeObject.stringValue)")
        }
    }

}

