//
//  CameraDeviceInput.swift
//  aCam
//
//  Created by Yaroslav Kupyak on 4/24/16.
//  Copyright © 2016 Yaroslav Kupyak. All rights reserved.
//

import UIKit
import AVFoundation

public enum CameraDeviceInputErrorType: ErrorType {
    case UnableToAddCamera
    case UnableToAddMic
}

class CameraDeviceInput {

    private var cameraDeviceInput: AVCaptureDeviceInput?
    private var micDeviceInput: AVCaptureDeviceInput?
    
    func configureInputCamera(session: AVCaptureSession, device: AVCaptureDevice) throws {
        let possibleCameraInput: AnyObject? = try AVCaptureDeviceInput(device: device)
        if let cameraInput = possibleCameraInput as? AVCaptureDeviceInput {
            if let currentDeviceInput = self.cameraDeviceInput {
                session.removeInput(currentDeviceInput)
            }
            self.cameraDeviceInput = cameraInput
            if session.canAddInput(self.cameraDeviceInput) {
                session.addInput(self.cameraDeviceInput)
            }
            else {
                throw CameraDeviceInputErrorType.UnableToAddCamera
            }
        }
    }
    
    func configureInputMic(session: AVCaptureSession, device: AVCaptureDevice) throws {
        if self.micDeviceInput != nil {
            return
        }
        try self.micDeviceInput = AVCaptureDeviceInput(device: device)
        if session.canAddInput(self.micDeviceInput) {
            session.addInput(self.micDeviceInput)
        }
        else {
            throw CameraDeviceInputErrorType.UnableToAddMic
        }
    }
}